angular.module('main.controllers')
	.controller('InitCtrl', function ($scope, $rootScope, $timeout, SemiPopup, RestApi, $stateParams, SemiStorage, SrifaAuth, $ionicHistory) {
		$scope.$on('$ionicView.enter', function(e) {
			$ionicHistory.nextViewOptions({
				disableBack: true
			});
			SrifaAuth.initializeApp();
		});
	})
;