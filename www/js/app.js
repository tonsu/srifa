// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('main', [
    'ionic',
    'main.controllers',
    'main.services',
    'main.directives',
    'semi.services',
    'semi.directives',
    'semi.filters'
])

    .run(function ($ionicPlatform, RestApi, $rootScope, $location, SrifaAuth, $state, SemiPopup, $timeout, $ionicHistory, $q, Helper, SemiStorage) {

        $rootScope.settings = {
            // mode: 'dev',
            mode: 'production',
            useBackgroundLocation: true,
            test: {
                noData: false // to show how it looks when there is no records (when calling API)
            },
            messageInterval: 6000,
            uuidInterval: 20000,
            // uuidInterval: 6000,
            chatInterval: 6000,
            chatNotificationInterval: 3000,
            testBgLocation: false,
            loginAfterRegister: true
        };

        // Global Variables
        // ----------------------------------------
        $rootScope.isTracking = false;
        $rootScope.deviceReady = false;

        // Common Functions
        // ----------------------------------------
        
        $rootScope.navigate = function(link) {
            // console.log('$location.url() link', $location.url() link);
            // console.log($location.url() + '/' + link);
            // $location.path($location.url() + '/' + link);
            $location.path(link);
        };

        $rootScope.toPreviousPage = function () {
            $ionicHistory.goBack();
        };

        // Execute every time url change
        // ----------------------------------------
        // $rootScope.$watch(
        //     function () {
        //         return $location.path();
        //     },
        //     function (a) {
        //         console.log('--- [url changed]', a);
        //         // show loading div, etc...
        //     }
        // );

        // Background Location
        // ----------------------------------------

        $rootScope.startTracking = function() {
            var authId = $rootScope.user ? $rootScope.user.authid : $rootScope.settings.testBgLocation ? 2 : 4; // dev: 2 for trackable
            console.log('- track authid: ', authId);
            var isTrackable = parseInt(authId) != 4;
            if($rootScope.settings.testBgLocation || ($rootScope.deviceReady && $rootScope.settings.useBackgroundLocation && window.BackgroundGeolocation && isTrackable)) {
                console.log('------ Location Enter Start Tracking: ');
                // Get a reference to the plugin.
                var bgGeo = window.BackgroundGeolocation;

                //This callback will be executed every time a geolocation is recorded in the background.
                var callbackFn = function(location, taskId) {
                    var coords = location.coords;
                    var lat    = coords.latitude;
                    var lng    = coords.longitude;

                    if(!$rootScope.settings.testBgLocation) { // forceStart is for debugging
                        if($rootScope.user.id && $rootScope.user.id.length > 1) {
                            console.log('- Location: ', JSON.stringify(location));
                            // Test Server
                            // RestApi.get('http://mspinfo.net/api-tester/public/api/set-location', {
                            //     aa: 'mock',
                            //     userid: $rootScope.user.id,
                            //     lat: lat,
                            //     log: lng
                            // });

                            // Srifa's Server
                            RestApi.post('http://srifaapp.appprompt.com/api/location/action_add_location', {
                                userid: $rootScope.user.id,
                                lat: lat,
                                log: lng
                            }).then(function(response) {
                                console.log('- location:userId', $rootScope.user.id);
                                console.log('- Location Sent:', JSON.stringify(response));
                            });
                        }
                    } else {
                        console.log('- Location Mock: ', JSON.stringify(location));
                        // Test Server
                        RestApi.get('http://mspinfo.net/api-tester/public/api/set-location', {
                            aa: 'mock',
                            userid: '007',
                            lat: lat,
                            log: lng
                        });
                    }

                    // Must signal completion of your callbackFn.
                    bgGeo.finish(taskId);
                };

                // This callback will be executed if a location-error occurs.  Eg: this will be called if user disables location-services.
                var failureFn = function(errorCode) {
                    console.warn('- BackgroundGeoLocation error: ', errorCode);
                };

                // Listen to location events & errors.
                bgGeo.on('location', callbackFn, failureFn);

                // Fired whenever state changes from moving->stationary or vice-versa.
                bgGeo.on('motionchange', function(isMoving, location) {
                    // console.log('location.mock', location.mock);
                    console.log('- onMotionChange: ', isMoving);
                });

                // BackgroundGeoLocation is highly configurable.
                bgGeo.configure({
                    // Geolocation config
                    desiredAccuracy: 0,
                    distanceFilter: 10,
                    stationaryRadius: 25,
                    locationUpdateInterval: 60*1000,
                    // fastestLocationUpdateInterval: 50000,

                    // Activity Recognition config
                    // activityType: 'AutomotiveNavigation',
                    // activityRecognitionInterval: 30000,
                    // stopTimeout: 0,

                    // Application config
                    debug: false,
                    stopOnTerminate: false,
                    startOnBoot: true,

                    // The must for AppStore
                    useSignificantChangesOnly: true,
                    // Hide Fitness Activity popup
                    disableMotionActivityUpdates: true,

                    // Android
                    foregroundService: true,
                    notificationTitle: 'Srifa กำลังใช้ตำแหน่งของคุณ',
                    notificationText: 'ตำแหน่งของคุณจะถูกแจ้งไปศรีฟ้า เพื่อความสะดวกในการรับบริการของท่าน',

                    // HTTP / SQLite config
                    // url: 'http://posttestserver.com/post.php?dir=cordova-background-geolocation',
                    // method: 'POST',
                    // autoSync: true,
                    // maxDaysToPersist: 1,
                    // headers: {
                    //     "X-FOO": "bar"
                    // },
                    // params: {
                    //     "auth_token": "maybe_your_server_authenticates_via_token_YES?"
                    // }
                }, function(state) {
                    // This callback is executed when the plugin is ready to use.
                    console.log('BackgroundGeolocation ready: ', state);
                    if (!state.enabled) {
                        bgGeo.start();
                    }
                });
            }
        };

        $rootScope.stopTracking = function() {
            if($rootScope.deviceReady && $rootScope.settings.useBackgroundLocation && window.BackgroundGeolocation) {
                console.log('------ Location Enter Stop Tracking: ');

                // Get a reference to the plugin.
                var bgGeo = window.BackgroundGeolocation;
                bgGeo.stop();
            }
        };

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            document.addEventListener('deviceready', onDeviceReady, false);

            // note: use '/init'
            // init for browser (dev)
            // if(!window.plugins) {
            //     SrifaAuth.initializeApp();
            // }

            function processMessage(data) {
                if (data && data.targetUrl) {
                    // var state = $injector.get($state);
                    // state.go(data.targetUrl);
                    var segments = data.targetUrl.split('/');

                    // --- Parse targetUrl
                    // e.g. 'pmt002'
                    var postType, postId;
                    console.log('segments:', segments);
                    if(segments.length == 1) {
                        console.log('segments[0]:', segments[0], segments[0].indexOf('nws') > -1);
                        if(segments[0].indexOf('nws') > -1) { // news
                            postType = 'news';
                        } else if(segments[0].indexOf('ms') > -1) { // chats
                            postType = 'chats';
                        } else { // promotions
                            postType = 'promotions';
                        }
                        postId = segments[0];
                    }
                    // e.g. 'promotions/pmt001'
                    else {
                        postType = segments[0];
                        postId = segments[1]
                    }
                    return {
                        postType: postType,
                        postId: postId
                    };
                    // console.log('postType:', postType);
                    // console.log('postId:', postId);
                }
                return null;
            }

            function redirectMessage(params) {
                if(params) {
                    if(params.postType == 'chats') {
                        $state.go('app.chats-content', {
                            id: params.postId
                        });
                    } else {
                        $state.go('app.posts-content', {
                            postType: params.postType,
                            id: params.postId
                        });
                    }
                }
            }

            // Login Promise
            $rootScope.loginDefer = $q.defer();

            // Plugins Initialization here
            function onDeviceReady() {

                $rootScope.deviceReady = true;
                console.log('device ready!');

                // note: use '/init'
                // fix: remove init route
                // SrifaAuth.initializeApp();

                // dev: test background location due to invalid license
                if($rootScope.settings.testBgLocation) {
                    console.log('- force start tracking');
                    $rootScope.startTracking();
                }

                if(window.plugins.OneSignal) {

                    // Android: fix handleNotificationReceived fired after handleNotificationOpened
                    // Note: not working
                    // $rootScope.notificationOpened = false;

                    console.log('plugin ready: OneSignal');
                    window.plugins.OneSignal
                        .startInit("40e3b8bd-ad5e-41ba-be8d-f87ac4f5ce38", "991069039710")
                        .handleNotificationReceived(function(jsonData) {
                            // Note: android will alert without having to use SemiPopup
                            // TargetURL: jsonData.payload.additionalData.targetUrl

                            // SemiPopup.alert('<div class="text-center">เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง + JSON.stringify(err)</div>');
                            // alert("Notification received:\n" + JSON.stringify(jsonData, null, 2));
                            // if(jsonData) {
                            //     SemiPopup.alert({
                            //         title: jsonData.payload.title,
                            //         template: '<div class="text-center">' + jsonData.payload.body + '</div>'
                            //     });
                            // }

                            // if(Helper.isIOS()) {
                            // if($rootScope.notificationOpened === false) {
                                if(jsonData) {
                                    $timeout(function() { // fix Popup
                                        var data = jsonData.payload.additionalData;
                                        // --- Check if the page is opening
                                        var params = processMessage(data);
                                        var currentPath = $location.path();
                                        if(currentPath.indexOf(params.postId) > -1) {
                                            // SemiPopup.alert({
                                            //     title: '1',
                                            //     template: '<div class="text-center">' + 'asdf' + '</div>'
                                            // });
                                            return;
                                        }

                                        // --- When the page is not opening
                                        SemiPopup.confirm({
                                            title: jsonData.payload.title,
                                            template: '<div class="text-center">' + jsonData.payload.body + '</div>',
                                            okText: 'เปิดอ่าน',
                                            cancelText: 'ปิด'
                                        }).then(function (isOk) {
                                            if (isOk) {
                                                redirectMessage(params);
                                            }
                                        });
                                    }, 7000);
                                }
                            // } else {
                            //     $rootScope.notificationOpened = false;
                            // }

                            // console.log('handleNotificationReceived: ' + JSON.stringify(jsonData));
                        })
                        .handleNotificationOpened(function(result) {
                            var data = result.notification.payload.additionalData;
                            $rootScope.loginDefer.promise.then(function () {
                                if(Helper.isAndroid()) {
                                    // $rootScope.notificationOpened = true;
                                }
                                var params = processMessage(data);
                                redirectMessage(params);
                            });
                        })
                        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
                        .endInit();

                    window.plugins.OneSignal.registerForPushNotifications();
                }
            }
        });

        // For development on browser (refresh without having to login)
        // ----------------------------------------
        $rootScope.lastUrl = $location.url();
        // console.log('$rootScope.lastUrl', $rootScope.lastUrl);
        $rootScope.initialized = false;
    })
;


