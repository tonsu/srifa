angular.module('main.directives', [])
	.directive('barcodeFooter', function (Helper, $location, $timeout, $ionicSideMenuDelegate, $rootScope) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			templateUrl: 'templates/components/barcode-footer.html',
			link: function (scope, element, attr) {
				scope.moreClasses = "";

				// Note: not working, execute before app-controller.js
				// console.log('$rootScope.user.anonymous --- ', $rootScope.user);
				// if($rootScope.user.anonymous) {
				// 	scope.moreClasses += "anonymous";
				// }
				// console.log('scope.moreClasses', scope.moreClasses);
				scope.showBarcode = function() {
					// todo: show barcode
				}
			}
		};
	})
;