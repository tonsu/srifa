angular.module('main.controllers')

	.controller('AppCtrl', function ($scope, $rootScope, $ionicModal, $timeout, $state, $location, RestApi, $ionicLoading, SemiPopup, Helper, SemiStorage, $ionicSideMenuDelegate) {

		// With the new view caching in Ionic, Controllers are only called
		// when they are recreated or on app start, instead of every page change.
		// To listen for when this page is active (for example, to refresh data),
		// listen for the $ionicView.enter event:
		//$scope.$on('$ionicView.enter', function(e) {
		//});

		resetForm();
		initializeApp();

		$scope.goToRegister = function () {
			$state.go("register");
		};

		// Go to Foget Password
		// ----------------------------------------
		$scope.goToForget = function () {
			$state.go("forget");
		};

		// Skip Login
		// ----------------------------------------
		$scope.skipLogin = function () {
			setSkipLoginUser();
			setContainerClasses();
			$state.go("app.home");
		};

		function setSkipLoginUser() {
			$rootScope.user = {
				id: "anonymous",
				surname: "ผู้ใช้ทั่วไป",
				lastname: "",
				username: "normal_user",
				email: "",
				carnumber: "",
				imgprofile: "/img/default-profile-img.png",
				imgsignature: "",
				barcode: "",
				membercode: "SRIFA00001",
				authid: "4",
				address: '',
				company: '',
				cartype: '',
				anonymous: true
			};
			console.log('$rootScope.user', $rootScope.user);
		}

		// Facebook Login
		// ----------------------------------------

		$scope.loginFacebook = function () {
			if (facebookConnectPlugin) {
				facebookConnectPlugin.getLoginStatus(function (response) {

					// Already logged in
					if (response.status === 'connected') {
						authenticate(response.authResponse)
					}
					// Not logged in, try logging in
					else {
						console.log('fbStatusSuccess', JSON.stringify(response));
						facebookConnectPlugin.login(['email', 'public_profile'], function (response) {
							// authenticate(response.authResponse.accessToken);
							authenticate(response.authResponse);
						}, function (err) {
							console.log('err', err);
							SemiPopup.alert('<div class="text-center">เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง + JSON.stringify(err)</div>');
						});

					}

					// $state.go("app.register");
					if (facebookConnectPlugin) {
						facebookConnectPlugin.login(['email'], fbLoginSuccess, fbLoginError);
					} else {
						console.log('---**--- no facebookConnectPlugin');
					}

				}, function (err) {
					console.log('fbStatusFail', JSON.stringify(err));
				});
			}
		};

		function fbLoginSuccess(response) {
			console.log('fbLoginSuccess', JSON.stringify(response));
			authenticate(response.authResponse);
			// facebookConnectPlugin.getAccessToken(function(token) {
			// 	console.log("Token: " + token);
			// });
			// getFacebookProfileInfo(response.authResponse) ;
		}

		function fbLoginError(error) {
			console.log('fbLoginError', JSON.stringify(error));
			SemiPopup.alert('<div class="text-center">เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง</div>');
		}

		// Check whether user already registered or not
		function authenticate(authResponse) {
			$rootScope.fbLoginOrRegister(authResponse);
		}

		function getFbUserDataAndRegister(authResponse) {
			// console.log('1.authResponse', JSON.stringify(authResponse));
			// console.log('authResponse.accessToken', authResponse.accessToken);
			facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
				function (response) {
					console.log('1.response', JSON.stringify(response));
					var fullName = response.name.split(' ');
					console.log('response.name', response.name);
					var firstName = fullName[0];
					var lastName = fullName[1] || '';
					$rootScope.fbRegisterParams = {
						fbId: authResponse.userID,
						email: response.email,
						firstName: firstName,
						lastName: lastName,
					};
					console.log('b4statego');
					$state.go("register");
				},
				function (response) {
					popupService.alert('Login with facebook fail!' + JSON.stringify(response));
				}
			);
		}

		$rootScope.fbLoginOrRegister = function (authResponse) {
			$ionicLoading.show();
			console.log('111facebookId', authResponse.userID);
			RestApi.post('http://srifaapp.appprompt.com/api/facebook/action_fb_login', {
				fb_id: authResponse.userID
			}).then(function (response) {
				console.log('facebook login check:', JSON.stringify(response));
				$ionicLoading.hide();

				// Already registered, logged in!
				if (response.data.result && response.data.result[0]) {
					console.log('fbLoginOrRegister: if');
					$rootScope.user = response.data.result[0];
					afterLoginSuccess();
					$state.go("app.home");
				}
				// Not registered, redirect to register
				else {
					console.log('fbLoginOrRegister: else');
					getFbUserDataAndRegister(authResponse);
				}
			}).finally(function () {
				$ionicLoading.hide();
			});
		};


		// Login Functions
		// ----------------------------------------

		$scope.login = function () {
			if ($rootScope.settings.mode == 'production') {
				$ionicLoading.show();
				RestApi.post('http://srifaapp.appprompt.com/api/login/get_action_login', {
					// user_id: $rootScope.user.id
					username: $scope.formData.username,
					password: $scope.formData.password
				}).then(function (response) {
					// console.log('login: response', response);
					$ionicLoading.hide();
					// console.log('response', $rootScope.user);
					if (response.data.result && response.data.result[0]) {
						$rootScope.user = response.data.result[0];
						afterLoginSuccess();
						$state.go("app.home");
					} else {
						SemiPopup.alert('<div class="text-center">ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง</div>');
					}
				}).finally(function () {
					$ionicLoading.hide();
				});
			} else {
				$state.go("app.home");
			}
			// $location.path('/app/posts/news');
		};

		function resetForm() {
			$scope.formData = {
				username: '',
				password: ''
			};
			// Dev: auto username
			if ($rootScope.settings.mode == 'dev') {
				$scope.formData = {
					username: 'username1',
					password: 'asdfasdf'
				};
			}
		}

		function storeUserAfterLogin() {
			var u = $rootScope.user;
			SemiStorage.set('id', u.id);
			SemiStorage.set('surname', u.surname);
			SemiStorage.set('lastname', u.lastname);
			SemiStorage.set('username', u.username);
			SemiStorage.set('email', u.email);
			SemiStorage.set('tel', u.tel);
			SemiStorage.set('carnumber', u.carnumber);
			SemiStorage.set('imgprofile', u.imgprofile);
			SemiStorage.set('imgsignature', u.imgsignature);
			SemiStorage.set('barcode', u.barcode);
			SemiStorage.set('membercode', u.membercode);
			SemiStorage.set('authid', u.authid);
			SemiStorage.set('address', u.address);
			SemiStorage.set('company', u.company);
			SemiStorage.set('cartype', u.cartype);
		}

		function getUserFromStorage() {
			return {
				id: SemiStorage.get('id'),
				surname: SemiStorage.get('surname') == "undefined" ? '' : SemiStorage.get('surname'),
				lastname: SemiStorage.get('lastname') == "undefined" ? '' : SemiStorage.get('lastname'),
				username: SemiStorage.get('username') == "undefined" ? '' : SemiStorage.get('username'),
				email: SemiStorage.get('email') == "undefined" ? '' : SemiStorage.get('email'),
				tel: SemiStorage.get('tel') == "undefined" ? '' : SemiStorage.get('tel'),
				carnumber: SemiStorage.get('carnumber') == "undefined" ? '' : SemiStorage.get('carnumber'),
				imgprofile: SemiStorage.get('imgprofile') == "undefined" ? '' : SemiStorage.get('imgprofile'),
				imgsignature: SemiStorage.get('imgsignature') == "undefined" ? '' : SemiStorage.get('imgsignature'),
				barcode: SemiStorage.get('barcode') == "undefined" ? '' : SemiStorage.get('barcode'),
				membercode: SemiStorage.get('membercode') == "undefined" ? '' : SemiStorage.get('membercode'),
				authid: SemiStorage.get('authid') == "undefined" ? '' : SemiStorage.get('authid'),
				address: SemiStorage.get('address') == "undefined" ? '' : SemiStorage.get('address'),
				company: SemiStorage.get('company') == "undefined" ? '' : SemiStorage.get('company'),
				cartype: SemiStorage.get('cartype') == "undefined" ? '' : SemiStorage.get('cartype'),
				anonymous: false
			};
		}

		// Note: check in template for more solid code
		// convert undefined to '-'
		// function undefinedStr(val) {
		// 	if(val === undefined) return val;
		// 	return val;
		// }

		function destroyUserFromStorage() {
			SemiStorage.remove(['id', 'surname', 'lastname', 'username', 'email', 'tel', 'carnumber', 'imgprofile', 'imgsignature', 'barcode', 'membercode', 'authid']);
		}

		// Logout
		// ----------------------------------------

		$scope.logout = function () {
			SemiPopup.confirm({
				title: 'Logout',
				template: '<div style="text-align: center">คุณกำลังออกจากระบบ!</div>'
			}).then(function (res) {
				// --- Background Geolocation
				$rootScope.stopTracking();
				if (res) { // clicked OK
					$ionicSideMenuDelegate.toggleLeft(false); // close left menu
					destroyUserFromStorage();
					$state.go("login");
				} else {
					// stay the same
				}
			});
		};

		// Initialize
		// ----------------------------------------

		function initializeApp() {
			// Check internet connection
			// ----------------------------------------
			console.log('init');

			$ionicLoading.show();
			// Get main branch info
			RestApi.get('http://srifaapp.appprompt.com/api/branch/get_branch').then(function (response) {
				$rootScope.srifa = response.data.result[0];
			}, function (err) { // error
				SemiPopup.alert('<div class="text-center">ไม่สามารถเชื่อมต่อกับระบบได้<br/>กรุณาเช็คการเชื่อมต่อ internet</div>');
				$state.go("login");
				console.log('error: initialize', err);
			}).finally(function () {
				$ionicLoading.hide();
			});

			// Process storage
			// ----------------------------------------

			var isAnonymous = $rootScope.user && $rootScope.user.anonymous;

			if ($rootScope.settings.mode == 'dev') {
				// Dev: mock logged in user
				$rootScope.user = {
					id: "usr0002",
					surname: "นายสมศักด์",
					lastname: "กินอะไรดี",
					username: "srifa1",
					email: "",
					tel: "08912345678",
					carnumber: "กบว123",
					imgprofile: "http://srifaapp.appprompt.com/uploads/user/usr0001.jpg",
					imgsignature: "http://srifaapp.appprompt.com/uploads/user/",
					barcode: "http://srifaapp.appprompt.com/uploads/barcode/srifa00001.gif",
					membercode: "SRIFA00001",
					authid: "1",
					address: '-',
					company: '-',
					cartype: '-'
				};
				// setSkipLoginUser();
			} else {
				// fix: skip login shouldn't use storage
				if (!isAnonymous) $rootScope.user = getUserFromStorage();
			}

			if (isAnonymous) {
				console.log('init: anonymous');
				$state.go("app.home");
			} else {
				console.log('init: local storage', $rootScope.user);
				if ($rootScope.user.id && $rootScope.user.authid) {
					afterLoginSuccess(true);
					// prevent going to home when app update
					if ($location.path() == '/login') {
						// if($location.path() == '/login' || $rootScope.settings.mode == 'production') {
						$state.go("app.home");
					}
				} else {
					// maybe go to login?
				}
			}

		}

		function setContainerClasses() {
			if ($rootScope.user.anonymous) {
				$rootScope.containerClasses = 'anonymous';
			} else {
				$rootScope.containerClasses = '';
			}
			console.log('$rootScope.containerClasses', $rootScope.containerClasses);
		}


		function afterLoginSuccess(alreadyLoggedIn) {
			console.log('After Login Success');
			// if(!alreadyLoggedIn) {
			// 	storeUserAfterLogin();
			// }
			// Prevent becoming anonymous
			$rootScope.user.anonymous = false;

			// Plugins
			// ----------------------------------------
			// todo: auth_id

			// --- OneSignal
			if(window.plugins && window.plugins.OneSignal) {
				console.log('send to OneSignal');
				window.plugins.OneSignal.sendTag("auth_id", "1");
			}

			// --- Background Geolocation
			$rootScope.startTracking();

			storeUserAfterLogin();
			setContainerClasses();
			resetForm();

			var now = new Date();

			// test add location
			// RestApi.post('http://srifaapp.appprompt.com/api/location/action_add_location', {
			// 	userid: $rootScope.user.id,
			// 	lat: 13.11112,
			// 	long: 13.11112
			// }).then(function(response) {
			// 	console.log('bg - location:', response);
			// });

			// commission: sum month
			RestApi.get('http://srifaapp.appprompt.com/api/transaction/get_transaction', {
				start: now.getMonth(), // #month
				end: now.getMonth() + 1,
				member: $rootScope.user.membercode
			}).then(function (response) {
				// var sum = 0;
				// for(var i in response.data.result) {
				// 	sum += parseInt(response.data.result[i].commission);
				// }
				// $scope.commissionSumMonth = sum;
				// console.log(response.data);
				if (response.data.commission_all) $scope.commissionSumAll = response.data.commission_all[0].sum;
				if (response.data.commission_by_month) $scope.commissionSumMonth = response.data.commission_by_month[0].sum;
			});
		}
	})
;