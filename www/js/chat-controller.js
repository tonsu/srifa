angular.module('main.controllers')

	.controller('ChatsCtrl', function ($scope, $rootScope, $timeout, $state, $stateParams, SemiPopup, RestApi, Helper, $interval) {
		$scope.$on('$ionicView.enter', function(e) {
			// Get Chat Data
			fetchChatData();
			$interval.cancel($rootScope.chatRefreshInterval);
			$rootScope.chatRefreshInterval = $interval(function() {
				fetchChatData();
			}, $rootScope.settings.chatInterval);
		});
		function fetchChatData() {
			RestApi.get('http://srifaapp.appprompt.com/api/message/get_contact', {
				user_id: $rootScope.user.id
			}).then(function(response) {
				$scope.chats = response.data.result;
				// console.log('$scope.chats', $scope.chats);
				for(var i in response.data.result) {
					var obj = response.data.result[i];
					obj.time = Helper.parseDate(obj.toTime); // convert time string to date object for Angular's date filter
				}
			});
		}
		$scope.goToChatRoom = function(params) {
			$state.go('app.chats-content', {
				id: params.id,
				// title: params.title,
				// time: params.toTime
			});
		};
		$scope.$on('$ionicView.leave', function(e) {
			$interval.cancel($rootScope.chatRefreshInterval);
		});
	})

	.controller('ChatsContentCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi, Helper, $ionicScrollDelegate, $interval) {

		console.log('$stateParams.id', $stateParams.id);
		$scope.$on('$ionicView.enter', function(e) {
			// Settings
			// --------------------
			$scope.data = {
				userId: $rootScope.user.id,
				contactId: $stateParams.id
			};
			$scope.header = {
				title: '-',
				dateTime: '-'
			};
			getAllMessages();
			// $scope.header = {
			// 	title: $stateParams.title,
			// 	dateTime: Helper.parseDate($stateParams.time)
			// };
			$interval.cancel($rootScope.messageRefreshInterval);
			$rootScope.messageRefreshInterval = $interval(function() {
				getAllMessages();
			}, $rootScope.settings.messageInterval);
		});

		$scope.sendMessage = function(message, userId, contactId) {
			$scope.message = '';
			RestApi.post('http://srifaapp.appprompt.com/api/message/action_post_message', {
				// user_id: $rootScope.user.id,
				user_id: $scope.data.userId,
				contact_id: $scope.data.contactId,
				message: message
			}).then(function(response) {
				getAllMessages();
				console.log('message: response', response);
			});
		};

		// Get Chat Data
		function getAllMessages() {
			RestApi.get('http://srifaapp.appprompt.com/api/message/get_message', {
				user: $scope.data.userId,
				contact: $scope.data.contactId
			}).then(function(response) {
				$ionicScrollDelegate.scrollBottom();
				$scope.content = response.data.result;
				if($scope.content) {
					// process each message
					for(var i in response.data.result) {
						var obj = response.data.result[i];
						obj.time = Helper.parseDate(obj.toTime); // convert time string to date object for Angular's date filter
						console.log('obj.time', obj.time);
						obj.avatarStyle = obj.action == 'U' ? {'background-image': "url('" + $rootScope.user.imgprofile + "')"} : {'background-image': "url('img/avatar-srifa.png')"};
					}
					console.log('$scope.content', $scope.header.title, $scope.content);
					$scope.header = {
						title: $scope.content[0].title,
						dateTime: Helper.parseDate($scope.content[0].toTime)
					}
				}
			});
		}

		$scope.$on('$ionicView.leave', function(e) {
			$interval.cancel($rootScope.messageRefreshInterval);
		});

	})


	// Plus Focus --------------------------

	.factory('focus', function($timeout, $window) {

		return function(id) {
			$timeout(function() {
				var element = $window.document.getElementById(id);
				if(element)
					element.focus();
			});
		};
	})
	.directive('eventFocus', function(focus) {
		return function(scope, elem, attr) {
			elem.on(attr.eventFocus, function() {
				focus(attr.eventFocusId);
			});
			scope.$on('$destroy', function() {
				elem.off(attr.eventFocus);
			});
		};
	})


;
