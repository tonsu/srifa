angular.module('main.controllers')

    .controller('ShopsCtrl', function ($scope, $timeout, $stateParams, SemiPopup, RestApi, $q, $rootScope, $ionicModal, $ionicScrollDelegate, $sce, Helper) {

        $scope.type = $stateParams.type;
        var srifaLocation = [13.974299, 99.675285];

        // ifame src [googlemap]-------------------------------
        //$scope.googleMapIframe = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3871.7361558689486!2d99.6730909507575!3d13.974304195814492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTPCsDU4JzI3LjUiTiA5OcKwNDAnMzEuMCJF!5e0!3m2!1sth!2sth!4v1482404501660';
        $scope.googleMapIframeUrl = $sce.trustAsResourceUrl("https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3871.7361558689486!2d99.6730909507575!3d13.974304195814492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTPCsDU4JzI3LjUiTiA5OcKwNDAnMzEuMCJF!5e0!3m2!1sth!2sth!4v1482404501660");

        // Get Map Data
        RestApi.get('http://srifaapp.appprompt.com/api/branch/get_branch').then(function (response) {
            $scope.content = response.data.result;
            srifaLocation = [parseFloat($scope.content[0].lat), parseFloat($scope.content[0].long)];
            //alert(srifaLocation);
            var newsMap = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3871.7361324208428!2d" + parseFloat($scope.content[0].long) + "!3d" + parseFloat($scope.content[0].lat) + "!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTPCsDU4JzI3LjUiTiA5OcKwNDAnMjMuMSJF!5e0!3m2!1sth!2sth!4v1482917562815";
            //console.log('newsMap', newsMap);
            $scope.googleMapIframeUrl = $sce.trustAsResourceUrl(newsMap);

            //$scope.googleMapIframeUrl = '';
            // Note: for displaying google map
            // $timeout(function() {
            // 	for(var i in $scope.content) {
            // 		var place = $scope.content[i];
            // 		var location = {lat: parseFloat(place.lat), lng: parseFloat(place.long)};
            // 		googleMapDefer.promise.then(function() {
            // 			console.log('location', location);
            // 			addMarker(location, map, i, $scope.content[i]);
            // 		});
            //
            // 	}
            //
            // }, 500);
            console.log('$scope.content', $scope.content);
        });

        $scope.callTel = function (tel) {
            window.location.href = 'tel:' + tel;
        };

        $scope.launchNavigator = function () {
            launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
                var app;
                if (isAvailable) {
                    app = launchnavigator.APP.GOOGLE_MAPS;
                    launchnavigator.navigate(srifaLocation, {
                        app: app
                    });
                } else {
                    // console.warn("Google Maps not available - falling back to user selection");
                    SemiPopup.alert({
                        title: 'ดาวน์โหลด',
                        template: '<div style="text-align: center">เพื่อความสะดวกในการใช้งาน กรุณาติดตั้ง Google Map</div>'
                    }).then(function() {
                        app = launchnavigator.APP.USER_SELECT;
                        launchnavigator.navigate(srifaLocation, {
                            app: app
                        });
                    });
                    // SemiPopup.confirm({
                    //     title: 'ดาวน์โหลด',
                    //     template: '<div style="text-align: center">เพื่อความสะดวกในการใช้งาน กรุณาติดตั้ง Google Map</div>',
                    //     cancelText: 'ข้าม',
                    //     okText: 'ติดตั้ง'
                    // }).then(function (isOk) {
                    //     if (isOk) { // clicked OK
                    //         if (Helper.isIOS()) {
                    //             // todo: AppStore
                    //         }
                    //     } else {
                    //         launchnavigator.navigate(srifaLocation);
                    //     }
                    // });

                }
            });

        };

        $scope.setType = function (type) {
            $scope.type = type; // not change top anymore
            if (type == 'google-map') {
                // launchnavigator.navigate(srifaLocation);
                // console.log('-- $scope.type', $scope.type);

                // Note: launch external navigator instead
                $ionicScrollDelegate.scrollTop();
                // $timeout(initGoogleMap, 100);

                var posOptions = {timeout: 10000, enableHighAccuracy: false};

                // Note: they don't need this
                // $cordovaGeolocation
                // 	.getCurrentPosition(posOptions)
                // 	.then(function (position) {
                // 		var locationMe = {lat: position.coords.latitude, lng: position.coords.longitude};
                // 		//alert('Lat:'+lat+"Long:"+long);
                // 		var data = [];
                // 		googleMapDefer.promise.then(function() {
                // 			var marker = new google.maps.Marker({
                // 				position: locationMe,
                // 				icon: 'img/marker2.png',
                // 				map: map
                // 			});
                // 			console.log('location', locationMe);
                // 		});
                //
                // 	}, function (err) {
                // 		// error
                // 		alert('ไม่พบพิกัด');
                // 	});

            } else if (type == 'map') {
            }
            // todo: nothing works
            // $scope.scroll = type == 'map';
            // $ionicScrollDelegate.getScrollView().options.scrollingY = type == 'map';

        };

        $ionicModal.fromTemplateUrl('templates/image-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.imageModal = modal;
            return modal;
        });

        $scope.openImageModal = function () {
            $scope.imageModal.show();
        };
        $scope.closeImageModal = function () {
            $scope.imageModal.hide();
        };

        // ----- Google Map Functions

        var map,
            isMapInitialized = false,
            googleMapDefer = $q.defer();

        if ($scope.type == 'google-map') initGoogleMap();

        function initGoogleMap() {
            // prevent create map more than once and initialize only when in `google-map` page
            if (isMapInitialized || $scope.type != 'google-map') {
                return;
            }

            var location = {lat: 14.0218, lng: 99.5334};
            map = new google.maps.Map(document.getElementById('map'), {
                center: location,
                // panControl: false,
                // zoomControl: false,
                scaleControl: true,
                mapTypeControl: false,
                // draggable: false,
                // scrollwheel: false,
                zoom: 7
            });

            google.maps.event.addDomListener(window, "resize", function () {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });

            isMapInitialized = true;
            googleMapDefer.resolve();
        }


        // Adds a marker to the map.
        function addMarker(location, map, index, data) {
            // Add the marker at the clicked location, and add the next-available label
            // from the array of alphabetical characters.

            var marker = new google.maps.Marker({
                position: location,
                icon: 'img/marker.png',
                map: map
            });
            var infoWindow = new google.maps.InfoWindow({
                // todo: use real data

                content: "<div id='info-window' class='info-window'>" +
                "<img src='" + data.img + "'>" +
                "<div class='title'>" + data.name + "</div>" +
                "<div class='address'>" + data.address + "</div>" +
                "</div>"
            });


            //old code
            // var infoWindow = new google.maps.InfoWindow({
            // 	// todo: use real data
            // 	content: "<div class='info-window'>" +
            // 	"<img src='" + data.img + "'>" +
            // 	"<div class='title'>" + data.name + "</div>" +
            // 	"<div class='address'>" + data.address + "</div>" +
            // 	"</div>"
            // });


            // "<div class='row pop-content'>" +
            // 	"<div class='col col-50 pop-left'><img src='"+data.img+"'></div>" +
            // 	"<div class='col col-50 pop-right'><p>"+data.name+"</p></div>" +
            // "</div>" +
            // "<div class='row pop-content'>" +
            // 	"<div class='col col-100'><p>"+data.address+"</p></div>" +
            // "</div>"

            // content: "<div class='row pop-content'><div class='col col-50 pop-left'><img ng-src='{{}}'></div><div class='col col-50 pop-right'><p>ศรีฟ้าเบเกอรี่</p><p>SRIFA BAKERY</p><p>สาขา : ศูนย์ของฝาก</p></div></div><div class='row pop-content2'><div class='title'><p>ที่อยู่ : 299 ม.4 ต.วังศาลา อ.ท่าม่วง จ.กาญจนบุรี 71110</p><p>โทรศัพท์ : 034613074</p><p>เว็บไซด์ : www.srifabakery.co.th</p><p>วัน-เวลาเปิด/ปิด : เปิดทุกวัน เวลา 07.00-20.00 น.</p></div></div>"

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open(map, marker);
            });
            google.maps.event.addDomListener(infoWindow, 'domready', function () {
                var info = document.getElementById("info-window");
                google.maps.event.addDomListener(info, "click", function () {
                    // console.log('1234', location);
                    if (launchnavigator) {
                        launchnavigator.navigate([location.lat, location.lng]);
                    }
                });
            });
            // old code
            // if(index == 0) {
            // 	infoWindow.open(map, marker);
            // 	map.setCenter(location);
            // }

            if (index == 0) {
                infoWindow.open(map, marker);
                map.setCenter(location);
            } else {
                infoWindow2.open(map, marker);
                map.setCenter(location);
            }
        }
    })
;
