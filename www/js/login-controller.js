angular.module('main.controllers')

	.controller('LoginCtrl', function ($scope, $rootScope, $ionicModal, $timeout, $state, SrifaAuth) {
		$scope.$on('$ionicView.enter', function(e) {
			$scope.resetForm();
			$scope.formData = {
				username: '',
				password: ''
			};
		});

		$scope.goToRegister = function () {
			$state.go("register");
		};

		$scope.goToForget = function () {
			$state.go("forget");
		};

		$scope.resetForm = function() {
			console.log('reset login form'); // miracle fix: no value in $scope.formData when calling `login`
			$scope.formData = {};
			$scope.formData = {
				username: '',
				password: ''
			};
			// Dev: auto username
			if ($rootScope.settings.mode == 'dev') {
				$scope.formData = {
					username: 'username1',
					password: 'asdfasdf'
				};
			}
		};

		// Skip Login
		// ----------------------------------------
		$scope.skipLogin = function () {
			SrifaAuth.skipLogin();
		};

		// Login
		// ----------------------------------------

		$scope.facebookLogin = function () {
			SrifaAuth.facebookLogin();
		};

		$scope.login = function (myForm) {
			console.log('myForm', myForm);
			console.log('$scope.formData', $scope.formData);
			SrifaAuth.login($scope.formData.username, $scope.formData.password);
		};
	})
;