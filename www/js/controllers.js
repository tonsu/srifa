angular.module('main.controllers', ['ngCordova'])
	
	// Home, News & Promotions
	// ----------------------------------------

	.controller('HomeCtrl', function ($scope, $rootScope, $timeout, SemiPopup, RestApi, $state) {
		$scope.$on('$ionicView.enter', function(e) {
			console.log('home: user', $rootScope.user);

			// --- prevent back button
			// from: https://codepen.io/calendee/pen/eusfl
			$scope.leftButtons = [{
				type: 'button-icon icon ion-navicon',
				tap: function(e) {
					$scope.sideMenuController.toggleLeft();
				}
			}];
			$scope.hideBackButton = true;

			// --- Fetch Data
			// News
			RestApi.post('http://srifaapp.appprompt.com/api/news/get_news', {
				auth_id: $rootScope.user.authid
			}).then(function(response) {
				$scope.news = response.data.result;
			});
			// Promotion
			RestApi.post('http://srifaapp.appprompt.com/api/promotion/get_promotion', {
				auth_id: $rootScope.user.authid
			}).then(function(response) {
				$scope.promotions = response.data.result;
			});

		});
	})
	.controller('PostsCtrl', function ($scope, $rootScope, $timeout, SemiPopup, RestApi, $stateParams) {
		$scope.$on('$ionicView.enter', function(e) {
			$scope.postType = $stateParams.postType;
			var url;
			if($scope.postType == 'news') {
				url = 'http://srifaapp.appprompt.com/api/news/get_news';
				$scope.viewTitle = 'ข้อมูลข่าวสาร';
			} else {
				url = 'http://srifaapp.appprompt.com/api/promotion/get_promotion';
				$scope.viewTitle = 'โปรโมชั่น / สินค้า';
			}
			RestApi.post(url, {
				auth_id: $rootScope.user.authid
			}).then(function(response) {
				console.log('response', response);
				$scope.postList = response.data.result;
				console.log('$scope.postList', $scope.postList);
			});
		});
	})
	.controller('PostsContentCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi, $cordovaSocialSharing, Helper) {
		$scope.$on('$ionicView.enter', function(e) {
			$scope.postType = $stateParams.postType;
			$scope.content = {};
			var contentId = $stateParams.id;
			var url;
			if($scope.postType == 'news') {
				url = 'http://srifaapp.appprompt.com/api/news/get_news_detail';
				$scope.header = {
					class: 'news',
					title: 'ข้อมูลข่าวสาร'
				};
			} else {
				url = 'http://srifaapp.appprompt.com/api/promotion/get_promotion_detail';
				$scope.header = {
					class: 'promotions',
					title: 'โปรโมชั่น / สินค้า'
				};
			}
			RestApi.get(url, {
				id: contentId
			}).then(function(response) {
				if(response.data.result) {
					$scope.content = response.data.result[0];
					// $scope.content.link = "http://www.srifabakery.co.th/"; // todo: ask for link from API
					console.log('$scope.content', $scope.content);
				} else {
					$scope.content = null;
				}

			});
		});

		// Share facebook with InAppBrowser
		$scope.shareFacebook = function() {
			var data = $scope.content;
			// Note: their request
			// var shareUrl = 'https://play.google.com/store/apps/details?id=com.appprompt.srifa'; // android
			// if (Helper.isIOS) {
			// 	shareUrl = 'https://itunes.apple.com/us/app/sri-fa-xea-thleth/id1187629213?ls=1&mt=8';
			// }
			// console.log('data.title', data.link);
			$cordovaSocialSharing
				.shareViaFacebook(null, null, data.link)
				// .shareViaFacebook(data.title, data.img, shareUrl)
				.then(function(result) {
					console.log('share: success', result);
					// Success!
				}, function(err) {
					console.log('share: failed', err);
					// An error occurred. Show a message to the user
				});
		};
	})

	// Barcode
	// ----------------------------------------

	.controller('BarcodeCtrl', function ($scope, $timeout, $stateParams, SemiPopup, RestApi, $rootScope) {
		// Note: get barcode from logged in data instead
		// RestApi.get('http://srifaapp.appprompt.com/api/profile/get_barcode', {
		// 	user: $rootScope.user.id
		// }).then(function(response) {
		// 	$scope.content = response.data.result[0];
		// 	console.log('$scope.content', $scope.content);
		// });
	})


	// Chats & Contact
	// ----------------------------------------

	.controller('ChatsCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi) {

		
	})

	.controller('ContactCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi) {
		$scope.$on('$ionicView.enter', function(e) {
			resetForm();
		});

		function resetForm() {
			$scope.formData = {
				fullName: '',
				tel: '',
				title: '',
				subject: ''
			};
		}

		$scope.submitContactForm = function() {
			var isAnonymous = $rootScope.user.anonymous;
			RestApi.post('http://srifaapp.appprompt.com/api/message/action_post_contact', {
				// user_id: $rootScope.user.id
				name: isAnonymous ? $scope.formData.fullName : $rootScope.user.surname + ' ' + $rootScope.user.lastname,
				tel: isAnonymous ? $scope.formData.tel : 'anonymous',
				title: $scope.formData.title,
				subject: $scope.formData.subject,
				user_id: isAnonymous ? 'anonymous' : $rootScope.user.id
			}).then(function(response) {
				if(response.data.status == true) {
					SemiPopup.alert('<div class="text-center">ส่งข้อความสำเร็จ</div>');
					resetForm();
				} else {
					SemiPopup.alert('<div class="text-center">ส่งข้อความเหลว<br/>กรุณาส่งใหม่อีกครั้ง</div>');
				}
				// console.log('submit contact: response', response);
			});
		};
	})

	// Commission
	// ----------------------------------------

	.controller('ProfileCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi) {
		$scope.resetForm = function() {
			$scope.formData = {
				oldPassword: '',
				newPassword: ''
			};
		};
		$scope.$on('$ionicView.enter', function(e) {
			console.log('$rootScope.user', $rootScope.user);
			var profileImg = $rootScope.user.imgprofile;
			console.log('$rootScope.user.imgprofile', $rootScope.user.imgprofile);
			if(profileImg.match(/\.(jpg|png|gif)\b/)) {
				$scope.profileImg = profileImg;
			} else {
				$scope.profileImg = '/img/default-profile-img.png'
			}
			$scope.resetForm();
		});

		console.log('$rootScope.user', $rootScope.user);

		$scope.changePassword = function() {
			// if($scope.formData.password != $scope.formData.passwordConfirm) {
			// 	SemiPopup.alert('<div class="text-center">รหัสผ่านไม่ตรงกัน</div>');
			// 	return;
			// }

			// console.log('- change pass:', JSON.stringify($scope.formData));
			
			// Change password only
			RestApi.post('http://srifaapp.appprompt.com/api/profile/action_post_profile', {
				user_id: $rootScope.user.id,
				new_password: $scope.formData.newPassword,
				old_password: $scope.formData.oldPassword,
				tel: ''
			}).then(function(response) {
				if(response.data.status == true) {
					SemiPopup.alert('<div class="text-center">เปลี่ยนรหัสผ่านสำเร็จ</div>');
					$scope.resetForm();
				} else {
					SemiPopup.alert('<div class="text-center">'+response.data.status+'</div>');
				}
				console.log('submit register: response', response);
			});
		};
	})

	// ForgetPassword
	// --------------------------------------------

	.controller('ForgetPasswordCtrl', function ($scope, $rootScope, $timeout, SemiPopup, RestApi,$state) {
		resetForm();

		function resetForm() {
			$scope.formData = {
				number: '',
				data: '',
				password: '',
				passwordConfirm: ''
			};
		}
		// submit ------------
		$scope.submitForgetForm = function() {
			// password not math ------------
			if($scope.formData.password != $scope.formData.passwordConfirm) {
				console.log('submit Forget: User :', $scope.formData.number,'| password :', $scope.formData.password);
				SemiPopup.alert('<div class="text-center">รหัสผ่านไม่ตรงกัน</div>');
				return;
			}
			// password is math ------------
			if($scope.formData.password == $scope.formData.passwordConfirm) {
	
				//Api-----------------------
				RestApi.post('http://srifaapp.appprompt.com/api/profile/action_post_forgotten_password', {
					number: $scope.formData.number,
					data: $scope.formData.data,
					password: $scope.formData.password
				}).then(function(response) {

					// todo: add more conditions
					if(response.data.result==true) {
						SemiPopup.alert('<div class="text-center">เปลี่ยนรหัสผ่านเรียบร้อย</div>').then(function() {
							// After click OK
							$state.go("login");
						});
						resetForm();
					} else if(response.data.result=='input false') {
						SemiPopup.alert('<div class="text-center">กรอกข้อมูลไม่ครบถ้วน</div>');
						resetForm();
					}else if(response.data.result=='user not found'){
						SemiPopup.alert('<div class="text-center">ไม่พบผู้ใช้งาน</div>');
						resetForm();
					} else {
						// !!!!!! Comment
						// You should always have `else` like this
						SemiPopup.alert('<div class="text-center">response.data.result</div>');
					}


					// if(response.data.result=='input false'){
					// 	SemiPopup.alert('<div class="text-center">กรอกข้อมูลไม่ครบถ้วน</div>');
					// 	resetForm();
					// }else if(response.data.result=='user not found'){
					// 	SemiPopup.alert('<div class="text-center">ไม่พบผู้ใช้งาน</div>');
					// 	resetForm();
					// }else if(response.data.result==true){
					// 	SemiPopup.alert('<div class="text-center">เปลี่ยนรหัสผ่านเรียบร้อย</div>');
					// 	resetForm();
					// 	$state.go("login");
					// }
					console.log('response: ', response);
				});
			}
		};
	})

;
