angular.module('semi.filters', ['semi.services'])
	.filter('reverse', function() {
		return function(items) {
			return items.slice().reverse();
		};
	})
	.filter('thaiDate', function(Helper) {
		return function(dateTime) {
			return Helper.getThaiDate(dateTime);
		};
	})
	.filter('thaiDate2', function(Helper) {
		return function(dateTime) {
			return Helper.getThaiDate2(dateTime);
		};
	})
	.filter('thaiTime', function(Helper) {
		return function(dateTime) {
			return Helper.getThaiTime(dateTime);
		};
	})
	.filter('thaiDateTime', function(Helper) {
		return function(dateTime) {
			return Helper.getThaiDateTime(dateTime);
		};
	})
	.filter('emptyToHyphen', function() {
		return function(value) {
			var val = parseInt(value);
			if(!val || val <= 0) return '-';
			return val;
		};
	})

	// Money
	.filter('thaiBaht', function() {
		return function(value) {
			var val = parseInt(value);
			if(!val || val <= 0) return '-';
			return val + ' บาท';
		};
	})
	.filter('couponUnit', function() {
		return function(value) {
			var val = parseInt(value);
			if(!val || val <= 0) return '-';
			return val + ' ใบ';
		};
	})
	.filter('srifaCommission', function(Helper) {
		return function(value) {
			var label = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
			var d = new Date(value);
			return label[d.getMonth()] + ' ' + (d.getFullYear() + 543);
		};
	})
;


