angular.module('main.controllers')

	.controller('CommissionsCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi) {
		$scope.$on('$ionicView.enter', function(e) {
			// console.log('-----', $rootScope.user);
			fetchData();
		});
		// Perform the login action when the user submits the login form
		$scope.filterMonth = function () {
			SemiPopup.alert('<div class="text-center">filtered</div>');
		};

		var now = new Date();
		$scope.now = now;
		// console.log('now', now.getMonth());

		$scope.data = {
			months: [
				{ name: 'มกราคม', id: 1},
				{ name: 'กุมภาพันธ์', id: 2},
				{ name: 'มีนาคม', id: 3},
				{ name: 'เมษายน', id: 4},
				{ name: 'พฤษภาคม', id: 5},
				{ name: 'มิถุนายน', id: 6},
				{ name: 'กรกฎาคม', id: 7},
				{ name: 'สิงหาคม', id: 8},
				{ name: 'กันยายน', id: 9},
				{ name: 'ตุลาคม', id: 10},
				{ name: 'พฤศจิกายน', id: 11},
				{ name: 'ธันวาคม', id: 12}
			]
		};
		// Year data, auto generated
		var dataYear = [];
		var maxYear = now.getFullYear();
		var minYear = 2016;
		var currentYear = maxYear;
		for(var i = 0; i < 20; i++) {
			dataYear.push({name: currentYear + 543, id: currentYear + 543});
			currentYear -= 1;
			if(currentYear < minYear) break;
		}
		$scope.data.years = dataYear;

		var startMonth = now.getMonth();
		var endMonth = (now.getMonth() + 1) % 12;

		// Fix: end and start of the year
		if(startMonth == 11 && endMonth == 0) {
			endMonth = 11; // both December
		} else if(startMonth == 0 && endMonth == 1) {
			endMonth = 0; // both January
		}

		// Form: Selects
		$scope.formData = {
			year: {id: maxYear + 543, name: maxYear + 543},
			start: {id: startMonth + 1, name: $scope.data.months[startMonth]},
			end: {id: endMonth + 1, name: $scope.data.months[endMonth]}
		};

		// Get Data
		function fetchData() {
			// console.log('-----2', $rootScope.user);
			RestApi.get('http://srifaapp.appprompt.com/api/transaction/get_transaction', {
				year: $scope.formData.year.id,
				start: $scope.formData.start.id, // #month
				end: $scope.formData.end.id,
				member: $rootScope.user.membercode
				// member: 'SRIFA00006'
			}).then(function(response) {
				$scope.commissions = response.data.result;
				console.log('$scope.commissions', $scope.commissions);
			});

			// // Sum: year
			// RestApi.get('http://srifaapp.appprompt.com/api/transaction/get_transaction', {
			// 	start: 1, // #month
			// 	end: 12,
			// 	member: $rootScope.user.membercode
			// }).then(function(response) {
			// 	var sum = 0;
			// 	for(var i in response.data.result) {
			// 		sum += parseInt(response.data.result[i].commission);
			// 	}
			// 	$scope.sumAll = sum;
			// });
		}

		$scope.changeMonth = function() {
			fetchData();
		}
	})
	.controller('CommissionsContentCtrl', function ($scope, $rootScope, $timeout, $stateParams, SemiPopup, RestApi) {
		$scope.$on('$ionicView.enter', function(e) {
			fetchData();
		});
		// Get Data
		function fetchData() {
			RestApi.get('http://srifaapp.appprompt.com/api/transaction/get_transaction_detail', {
				id: $stateParams.commissionId
			}).then(function(response) {
				$scope.commission = response.data.result[0];
				console.log('$scope.commission', $scope.commission);
			});
		}
	})
;
