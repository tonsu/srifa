angular.module('semi.services', ['ionic'])
	/**
	 * A simple example service that returns some data.
	 */
	.factory('RestApi', function ($http, $log, $rootScope, SemiPopup, $ionicLoading) {
		function encodePayload(obj) {
			var str = [];
			for(var p in obj)
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			return str.join("&");
		}

		// process response data before returning
		function filterResponse(response, url) {

			// noData middleware
			// --------------------
			if($rootScope.settings.test.noData) {
				var noDataList = [
					'http://srifaapp.appprompt.com/api/news/get_news',
					'http://srifaapp.appprompt.com/api/promotion/get_promotion',
					'http://srifaapp.appprompt.com/api/transaction/get_transaction',
				];
				if(noDataList.indexOf(url) >= 0) {
					return {data: {result: null}};
				}
			}
			return response;
		}

		return {
			get: function (url, data) {
				if(data) {
					url = url + '?' + encodePayload(data);
				}
				return $http.get(url).then(function(response) {
					return filterResponse(response, url);
				}, function(error) {
					return error;
				});
			},
			post: function (url, data) {
				var req = {
					method: 'POST',
					url: url,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					// Fix content type, see: http://stackoverflow.com/questions/24710503/how-do-i-post-urlencoded-form-data-with-http-in-angularjs
					transformRequest: encodePayload,
					data: data
				};
				return $http(req).then(function(response){
					$ionicLoading.hide();
					return filterResponse(response, url);
				}, function(error){
					$ionicLoading.hide();
					return error;
				});


				// Note: below is for content type == json
				// console.log('sending: post', url);
				// return $http.post(url, data).then(function (response) {
				// 	//console.log("response : "+JSON.stringify(response));
				// 	$ionicLoading.hide();
				// 	$rootScope.loading = false;
				// 	// if (response.data.status == "success") {
				// 	// 	$ionicHistory.goBack();
				// 	// } else {
				// 	// 	SemiPopup.alert(JSON.stringify(response.data.message));
				// 	// }
				// }, function (response) {
				// 	$ionicLoading.hide();
				// 	$rootScope.loading = false;
				// 	console.log('error msg : ' + JSON.stringify(response));
				// 	SemiPopup.alert('Please check your internet connection.');
				// });
			}
		}
	})

	.factory('SemiPopup', function ($ionicPopup, $rootScope) {
		return {
			show: function (param, scope) {
				if (!scope) scope = $rootScope.$new();
				scope.data = {};

				return $ionicPopup.show({
					title: param.title, // String. The title of the popup.
					cssClass: param.cssClass, // String, The custom CSS class name
					subTitle: param.subTitle, // String (optional). The sub-title of the popup.
					template: param.template, // String (optional). The html template to place in the popup body.
					templateUrl: param.templateUrl, // String (optional). The URL of an html template to place in the popup   body.
					scope: scope, // Scope (optional). A scope to link to the popup content.
					buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
						text: param.buttons.cancelText,
						type: param.buttons.cancelType,
						onTap: function (e) {
							return false;
						}
					}, {
						text: param.buttons.okText,
						type: param.buttons.okType,
						onTap: function (e) {
							// Returning a value will cause the promise to resolve with the given value.
							return scope.data.value;
						}
					}]
				});
			},
			alert: function (param) {
				if(typeof param == 'object') {
					return $ionicPopup.alert({
						title: param.title,
						template: param.template,
						okText: 'ตกลง'
					});
				} else {
					return $ionicPopup.alert({
						template: param,
						okText: 'ตกลง'
					});
				}

			},
			confirm: function (param) {
				return $ionicPopup.confirm({
					title: param.title,
					template: param.template,
					cancelText: param.cancelText || 'ยกเลิก',
					okText: param.okText || 'ตกลง'
				});
			}
		};
	})

	.factory('Bookmark', function (CacheFactory) {

		if (!CacheFactory.get('bookmarkCache')) {
			CacheFactory.createCache('bookmarkCache');
		}

		var bookmarkCache = CacheFactory.get('bookmarkCache');

		return {
			set: function (id) {
				bookmarkCache.put(id, 'bookmarked');
			},
			get: function (id) {
				bookmarkCache.get(id);
			},
			check: function (id) {
				var keys = bookmarkCache.keys();
				var index = keys.indexOf(id);
				if (index >= 0) {
					return true;
				} else {
					return false;
				}
			},
			remove: function (id) {
				bookmarkCache.remove(id);
			}
		}

	})

	.factory('SemiStorage', ['$window', function ($window) {
		return {
			set: function (key, value) {
				$window.localStorage[key] = value;
			},
			get: function (key, defaultValue) {
				var result = $window.localStorage[key] || defaultValue;
				return result == 'undefined' ? '' : result;
			},
			setObject: function (key, value) {
				$window.localStorage[key] = JSON.stringify(value);
			},
			getObject: function (key) {
				return JSON.parse($window.localStorage[key] || '{}');
			},
			remove: function(keys) {
				for(var i in keys) {
					var key = keys[i];
					$window.localStorage.removeItem(key);
				}
			},
			clear: function(keys) {
				$window.localStorage.clear();
			}
		}
	}])
;


