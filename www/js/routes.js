angular.module('main')
	.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

		// see: http://ionicframework.com/docs/api/provider/%24ionicConfigProvider/
		$ionicConfigProvider.backButton.text('กลับ').previousTitleText(false);

		$stateProvider
			.state('app', {
				url: '/app',
				abstract: true,
				templateUrl: 'templates/menu.html',
				controller: 'AppCtrl',
				// wait for ionic.Platform.ready
				// https://github.com/driftyco/ng-cordova/issues/8
				resolve: {
					cordova: function($q) {
						var deferred = $q.defer();
						ionic.Platform.ready(function() {
							console.log('[route] ionic.Platform.ready');
							deferred.resolve();
						});
						return deferred.promise;
					}
				}
			})

			// User Account
			// ----------------------------------------

			.state('init', {
				url: '/init',
				templateUrl: 'templates/init.html',
				controller: 'InitCtrl',
				resolve: {
					cordova: function($q) {
						var deferred = $q.defer();
						ionic.Platform.ready(function() {
							console.log('ionic.Platform.ready');
							deferred.resolve();
						});
						return deferred.promise;
					}
				}
			})
			.state('login', {
				url: '/login',
				templateUrl: 'templates/login.html',
				controller: 'LoginCtrl'
			})
			.state('register', {
				url: '/register',
				controller: 'RegisterCtrl',
				templateUrl: 'templates/register.html'
			})
			// Old Requirement: facebook register doesn't require username and password
			// .state('register-facebook', {
			// 	url: '/register/:isFacebook',
			// 	controller: 'RegisterCtrl',
			// 	templateUrl: 'templates/register-facebook.html'
			// })
			.state('forget', {
				url: '/forget',
				controller: 'ForgetPasswordCtrl',
				templateUrl: 'templates/forget-password.html'
			})
			.state('app.barcode', {
				url: '/barcode',
				views: {
					'menuContent': {
						controller: 'BarcodeCtrl',
						templateUrl: 'templates/barcode.html'
					}
				}
			})

			// Home, News & Promotions
			// ----------------------------------------

			.state('app.home', {
				url: '/home',
				views: {
					'menuContent': {
						controller: 'HomeCtrl',
						templateUrl: 'templates/home.html'
					}
				}
			})
			.state('app.posts', {
				url: '/posts/:postType',
				views: {
					'menuContent': {
						controller: 'PostsCtrl',
						templateUrl: 'templates/posts.html'
					}
				}
			})
			.state('app.posts-content', {
				url: '/posts/:postType/:id',
				views: {
					'menuContent': {
						controller: 'PostsContentCtrl',
						templateUrl: 'templates/content.html'
					}
				}
			})

			// Chats (Inbox)
			// ----------------------------------------

			.state('app.chats', {
				url: '/chats',
				views: {
					'menuContent': {
						controller: 'ChatsCtrl',
						templateUrl: 'templates/chats.html'
					}
				}
			})
			.state('app.chats-content', {
				url: '/chats/:id',
				views: {
					'menuContent': {
						controller: 'ChatsContentCtrl',
						templateUrl: 'templates/chats-content.html'
					}
				}
			})
			// Note: get title and time from first message instead
			// .state('app.chats-content', {
			// 	url: '/chats/:id/:title/:time',
			// 	views: {
			// 		'menuContent': {
			// 			controller: 'ChatsContentCtrl',
			// 			templateUrl: 'templates/chats-content.html'
			// 		}
			// 	}
			// })

			// Shops (Map)
			// ----------------------------------------

			.state('app.shops', {
				url: '/shops/:type',
				views: {
					'menuContent': {
						controller: 'ShopsCtrl',
						templateUrl: 'templates/shops.html'
						// templateUrl: 'templates/page_map-01.html',
					}
				}
			})
			.state('app.image-modal', {
				url: '/image-modal',
				templateUrl: 'templates/image-modal.html'
			})

			// Commissions
			// ----------------------------------------

			.state('app.commissions', {
				url: '/commissions',
				views: {
					'menuContent': {
						controller: 'CommissionsCtrl',
						templateUrl: 'templates/commissions.html'
					}
				}
			})
			.state('app.commissions-content', {
				url: '/commissions/:commissionId',
				views: {
					'menuContent': {
						controller: 'CommissionsContentCtrl',
						templateUrl: 'templates/commissions-content.html'
					}
				}
			})

			// Contact
			// ----------------------------------------

			.state('app.contact', {
				url: '/contact',
				views: {
					'menuContent': {
						controller: 'ContactCtrl',
						templateUrl: 'templates/contact.html'
					}
				}
			})

			// Profile
			// ----------------------------------------

			.state('app.profile', {
				url: '/profile',
				views: {
					'menuContent': {
						controller: 'ProfileCtrl',
						templateUrl: 'templates/profile.html'
					}
				}
			})

			// .state('app.profilemock', {
			// 	url: '/profilemock',
			// 	views: {
			// 		'menuContent': {
			// 			controller: 'ProfileCtrl',
			// 			templateUrl: 'templates/profile-mock.html'
			// 		}
			// 	}
			// })

			// Example (for learning SCSS)
			// ----------------------------------------

			.state('app.example', {
				url: '/example',
				views: {
					'menuContent': {
						templateUrl: 'templates/example.html'
					}
				}
			})
		;
		// $urlRouterProvider.otherwise('/login');
		$urlRouterProvider.otherwise('/init');
	})
;