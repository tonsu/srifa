angular.module('main.controllers')

	.controller('AppCtrl', function ($scope, $rootScope, $ionicModal, $timeout, $state, $location, RestApi, $ionicLoading, SemiPopup, Helper, SemiStorage, $ionicSideMenuDelegate, SrifaAuth, $interval) {
		// With the new view caching in Ionic, Controllers are only called
		// when they are recreated or on app start, instead of every page change.
		// To listen for when this page is active (for example, to refresh data),
		// listen for the $ionicView.enter event:
		//$scope.$on('$ionicView.enter', function(e) {
		//});

		// console.log('window.innerWidth', window.innerWidth);
		// console.log('window.innerHeight', window.innerHeight);

		// alert(window.innerWidth + ', ' + window.innerHeight);

		// Logout
		// ----------------------------------------

		$scope.logout = function () {
			SrifaAuth.logout();
		};

		$scope.goToLogin = function () {
			$state.go("login");
		};

		$scope.goToRegister = function () {
			$state.go("register");
		};
		
		SrifaAuth.initializeApp();

		// todo: chat notification
		// function updateChatNotification() {
		// 	if($rootScope.user && !$rootScope.user.anonymous) {
		// 		// console.log('$rootScope.user', $rootScope.user);
		// 	}
		// }
		//
		// $scope.chatRefreshInterval = $interval(function() {
		// 	updateChatNotification();
		// }, $rootScope.settings.chatNotificationInterval);
	})
;