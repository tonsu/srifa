angular.module('main.controllers')
	.controller('RegisterCtrl', function ($scope, $rootScope, $timeout, SemiPopup, RestApi, $state, SrifaAuth, $ionicHistory, $stateParams) {
		$scope.$on('$ionicView.enter', function(e) {
			resetForm();
			console.log('enter: register', JSON.stringify($rootScope.fbRegisterParams));
			if($rootScope.fbRegisterParams) {
				$scope.fbId = $rootScope.fbRegisterParams.fbId;
				$scope.formData.name = $rootScope.fbRegisterParams.firstName;
				$scope.formData.lastname = $rootScope.fbRegisterParams.lastName;
				$scope.formData.email = $rootScope.fbRegisterParams.email;
				$rootScope.fbRegisterParams = false;
			}
			// console.log('$stateParams.isFacebook', $stateParams.isFacebook);
			$scope.isFacebook = $stateParams.isFacebook == 'true';
			if($scope.isFacebook) {
				console.log('222333', 222333);
			}
		});
		function resetForm() {
			$scope.formData = {
				username: '',
				password: '',
				passwordConfirm: '',
				name: '',
				lastname: '',
				email: '',
				tel: '',
				fb_id: ''
			};
		}
		$scope.backFromRegister = function() {
			if($ionicHistory.backView()) {
				if($ionicHistory.backView().url == '/login') {
					$state.go("login", {}, {location: "replace", reload: true});
				} else {
					$ionicHistory.goBack();
				}
			} else {
				$state.go("login", {}, {location: "replace", reload: true});
				//$ionicHistory.goBack();
			}
			// console.log('$ionicHistory.backView() ', $ionicHistory.backView().url );
		};

		$scope.submitRegisterForm = function() {
			// Check matching password
			if($scope.formData.password != $scope.formData.passwordConfirm) {
				SemiPopup.alert('<div class="text-center">รหัสผ่านไม่ตรงกัน</div>');
				return;
			}

			// console.log('- regis:', JSON.stringify($scope.formData));

			var registerData = $scope.isFacebook ? {
				name: $scope.formData.name,
				lastname: $scope.formData.lastname,
				email: $scope.formData.email,
				tel: $scope.formData.tel,
				fb_id: $scope.fbId ? $scope.fbId : ''
			} : {
				username: $scope.formData.username,
				password: $scope.formData.password,
				name: $scope.formData.name,
				lastname: $scope.formData.lastname,
				email: $scope.formData.email,
				tel: $scope.formData.tel,
				fb_id: $scope.fbId ? $scope.fbId : ''
			};

			RestApi.post('http://srifaapp.appprompt.com/api/register/get_register', registerData).then(function(response) {
				if(response.data.status == true) {
					SemiPopup.alert('<div class="text-center">สมัครสำเร็จ</div>').then(function() {

						// Note: old logic
						// if($rootScope.facebookId) {
						// 	$rootScope.loginWithFacebookId($rootScope.facebookId);
						// } else {
						// 	// Auto login after registered
						// 	if($rootScope.settings.loginAfterRegister) {
						// 		SrifaAuth.login($scope.formData.username, $scope.formData.password);
						// 	} else {
						// 		$state.go("login");
						// 	}
						// 	resetForm();
						// }

						// Auto login after registered
						if($rootScope.settings.loginAfterRegister) {
							SrifaAuth.login($scope.formData.username, $scope.formData.password);
							resetForm();
						} else {
							resetForm();
							$state.go("login");
						}
					});
				} else if(response.data.status == 'false duplicate user') {
					SemiPopup.alert('<div class="text-center">ชื่อผู้ใช้ (ID) นี้ถูกสมัครแล้ว กรุณาใช้ชื่อผู้ใช้อื่น</div>');
				} else if(response.data.status == 'false duplicate facebook id') {
					SemiPopup.alert('<div class="text-center">Facebook ID นี้ถูกสมัครแล้ว กรุณาใช้ Facebook ID อื่น</div>');
				} else {
					// console.log('333 resister response', JSON.stringify(response));
					if(response.data.status) {
						SemiPopup.alert('<div class="text-center">'+response.data.status+'</div>');
					} else {
						SemiPopup.alert('<div class="text-center">การติดต่อกับ server มีปัญหา กรุณาลองใหม่อีกครั้ง</div>');
					}

					// SemiPopup.alert('<div class="text-center">สมัครล้มเหลว<br/>กรุณาลองใหม่อีกครั้ง</div>');
				}
				console.log('submit register: response', JSON.stringify(response));
			});
		};
	})
;