angular.module('semi.directives', ['semi.services'])

	.directive('graphic', function (Helper, $location, $timeout) {
		return {
			restrict: 'AE',
			replace: true,
			transclude: true,
			scope: {},
			template: '<div class="graphic-wrap" ng-class="moreClasses" ng-transclude></div>',
			link: function (scope, element, attr) {
				var elem = element[0];

				function calculateFontSize() {
					var elemWidth = parseFloat(Helper.getComputedStyle(elem, 'width'));
					elem.style.fontSize = ((elemWidth / 100) * 3) + 'px'; // multiply by 3 to minimize the font too big effect
				}

				function onResize() {
					var timeout = $timeout(calculateFontSize, 1, false); // 1000 = 1sec
				}

				function cleanUp() {
					angular.element(window).off('resize', onResize);
				}

				angular.element(window).on('resize', onResize);
				scope.$on('$destroy', cleanUp);
				onResize();
			}
		};
	})

	.directive('semiNavBar', function (Helper, $location, $timeout, $ionicSideMenuDelegate) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			templateUrl: 'templates/semi/semi-nav-bar.html',
			link: function (scope, element, attr) {
				scope.moreClasses = "";
				scope.toggleSideMenu = function () {
					$ionicSideMenuDelegate.toggleLeft();
				}
			}
		};
	})

	.directive('semiButton', function (Helper, $location, $timeout, $ionicSideMenuDelegate) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			transclude: true,
			template: '<div class="semi-btn" ng-class="moreClasses">' +
			'<img ng-if="imgLeftSrc" class="img-icon icon-left" ng-src="{{imgLeftSrc}}" />' +
			'<div class="btn-content" ng-transclude></div>' +
			'<img ng-if="imgRightSrc" class="img-icon icon-right" ng-src="{{imgRightSrc}}" align="middle" />' +
			'</div>',
			link: function (scope, element, attr) {
				scope.moreClasses = "";
				if (attr.fullWidth !== undefined) {
					scope.moreClasses += ' full-width';
				}
				if (attr.type == 'outline') {
					scope.moreClasses += ' outline';
				}
				if (attr.type == 'outline-inverse') {
					scope.moreClasses += ' outline-inverse';
				} else if (attr.type == 'secondary') {
					scope.moreClasses += ' secondary';
				} else if (attr.type == 'gray') {
					scope.moreClasses += ' gray';
				}
				if (attr.hasLink !== undefined) {
					scope.moreClasses += ' has-link';
				}

				// Image Icon
				if (attr.imgLeft) {
					scope.imgLeftSrc = attr.imgLeft;
				}
				if (attr.imgRight) {
					scope.imgRightSrc = attr.imgRight;
				}

				// Style
				if (attr.rounded !== undefined) {
					scope.moreClasses += ' rounded';
				}
			}
		};
	})

	.directive('semiSubHeader', function (Helper, $location, $timeout, $ionicSideMenuDelegate) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			transclude: true,
			template: '<div class="semi-sub-header" ng-style="{top: rootStyle.navBarHeight + \'px\'}" ng-class="moreClasses" ng-transclude></div>',
			link: function (scope, element, attr) {
				scope.moreClasses = "";
			}
		};
	})

	.directive('semiPostItem', function (Helper, $location, $timeout, $ionicSideMenuDelegate) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			transclude: true,
			templateUrl: 'templates/semi/semi-post-item.html',
			link: function (scope, element, attr) {
				scope.moreClasses = "";
				scope.thumbnail = attr.thumbnail || 'img/img-placeholder.png';
				scope.href = attr.href;
			}
		};
	})

	.directive('semiImgIcon', function (Helper, $location, $timeout, $ionicSideMenuDelegate) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			template: '<div class="semi-img-icon" ng-class="moreClasses" href="{{href}}" ng-style="style"></div>',
			link: function (scope, element, attr) {
				scope.moreClasses = "";
				scope.src = attr.src || 'img/img-placeholder.png';
				scope.href = attr.href;

				// Auto width & height
				var elem = element[0]
				var parentHeight = parseFloat(Helper.getComputedStyle(elem.parentNode, 'height'));
				// console.log('parentHeight', parentHeight);

				scope.style = {
					'background-image': 'url(' + attr.src + ')' || 'url(img/img-placeholder.png)',
					'background-position': attr.align || 'center center',
					'background-size': attr.scale ? attr.scale + ' auto' : '75% auto',
					'background-repeat': attr.repeat || 'no-repeat',
					'position': attr.absolute || 'absolute',
					'top': attr.top || 'auto',
					'left': attr.left || 'auto',
					'right': attr.right || 'auto',
					'bottom': attr.bottom || 'auto',
					'width': attr.width || parentHeight + 'px',
					'padding-bottom': attr.width || 'auto',
					'height': attr.height || parentHeight + 'px'
				}
			}
		};
	})

	.directive('semiNoRecord', function (Helper, $location, $timeout, $ionicSideMenuDelegate) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {},
			template: '<div class="semi-no-record">ไม่พบรายการ</div>',
			link: function (scope, element, attr) {
			}
		};
	})

	/**
	 * Fix bagkgound-image url not found, same as ng-srd
	 * from: http://stackoverflow.com/questions/13781685/angularjs-ng-src-equivalent-for-background-imageurl
	 */
	.directive('semiBgImg', function () {
		return function (scope, element, attrs) {
			var url = attrs.semiBgImg;
			element.css({
				'background-image': 'url(' + url + ')'
			});
		};
	})
	/**
	 * 404 fallback image
	 * see: http://stackoverflow.com/questions/16310298/if-a-ngsrc-path-resolves-to-a-404-is-there-a-way-to-fallback-to-a-default
	 */
	.directive('errSrc', function () {
		return {
			link: function (scope, element, attrs) {
				element.bind('error', function () {
					if (attrs.src != attrs.errSrc) {
						attrs.$set('src', attrs.errSrc);
					}
				});
			}
		}
	})
	/**
	 * form input charactor length limit
	 */
	.directive("limitTo", [function () {
		return {
			restrict: "A",
			link: function (scope, elem, attrs) {
				var limit = parseInt(attrs.limitTo);
				// angular.element(elem).on("keypress", function (e) {
				angular.element(elem).on("keydown", function (e) {
					if (this.value.length == limit && e.keyCode !== 8) e.preventDefault();
				});
			}
		}
	}]);
;