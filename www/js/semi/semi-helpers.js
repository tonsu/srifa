angular.module('semi.services')
    .factory('Helper', function ($location) {
        var fac = {};
        fac.getComputedStyle = function(elem, propName){
            // var elem = document.getElementById("elem-container");
            var theCSSprop = window.getComputedStyle(elem,null).getPropertyValue(propName);
            return theCSSprop;
        };
        fac.isTablet = function(){
            var winWidth = window.innerWidth;
            return winWidth >= 768;
        };
        fac.isRatio_3_4 = function(){
            return window.innerWidth / window.innerHeight == 3/4;
        };
        fac.isRatio_2_3 = function(){
            return window.innerWidth / window.innerHeight == 2/3;
        };
        fac.getGameName = function(){
            var url = $location.url().split('/');
            return url[2];
        };
		/**
         * for checking attribute (e.g. <tag attribute />)
         */
        fac.isSet = function(obj){
            return obj !== undefined && obj !== false;
        };
		/**
         * Array
         */
        // not yet tested (still use shuffleClone)
        fac.shuffleArray = function (a) {
            var j, x, i;
            for (i = a.length; i; i--) {
                j = Math.floor(Math.random() * i);
                x = a[i - 1];
                a[i - 1] = a[j];
                a[j] = x;
            }
        };
        /**
         * DateTime
         */
        fac.humanizeTime = function (input, units, hasSpace) {
            // units is a string with possible values of y, M, w, d, h, m, s, ms
            hasSpace = hasSpace === undefined ? true : hasSpace;
            if (input == 0) {
                if (hasSpace) return "00 : 00";
                return "00:00";
            } else {
                var duration = moment().startOf('day').add(input, units);
                var format = "";
                if (duration.hour() > 0) {
                    if (hasSpace) format += "H : ";
                    else format += "H:";
                }
                if (hasSpace) format += "mm : ss";
                else format += "mm:ss";
                return duration.format(format);
            }
        };
        /**
         * fix incorrect date format for Safari, Firefox
         */
        fac.parseDate = function(input, format) {
            if(!input) return input;
            return new Date(input.replace(/-/g, "/"));

            // more complex: but only parse date
            // ref: http://stackoverflow.com/questions/3085937/safari-js-cannot-parse-yyyy-mm-dd-date-format

            // format = format || 'yyyy-mm-dd'; // default format
            // var parts = input.match(/(\d+)/g),
            //     i = 0, fmt = {};
            // // extract date-part indexes from the format
            // format.replace(/(yyyy|dd|mm)/g, function(part) { fmt[part] = i++; });
            //
            // return new Date(parts[fmt['yyyy']], parts[fmt['mm']]-1, parts[fmt['dd']]);
        };
        fac.getThaiDate = function (dateTime) {
            var d = new Date(dateTime);
            var monthLabels = ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.', 'พ.ย.','ธ.ค.'];
            // console.log('srcDate', srcDate);
            // var d=srcDate.getDate(),
            //     m=srcDate.getMonth() + 1,
            //     y=srcDate.getFullYear() + 543,
            //     hour=srcDate.getHours(),
            //     min=srcDate.getMinutes(),
            //     sec=srcDate.getSeconds();

			// return d.getDate() + ' ' +  monthLabels[d.getMonth()] + ' ' + (d.getYear() - 100 + 43);
			return d.getDate() + ' ' +  monthLabels[d.getMonth()];
        };
        fac.getThaiDate2 = function (dateTime) {
            var d = new Date(fac.parseDate(dateTime));
            if(isNaN(d.getMonth())) return ''; // prevent flashing NaN
            var month = d.getMonth() + 1;
            if(month < 10) month = '0' + month;
			return d.getDate() + '/' +  month + '/' + (d.getFullYear() + 543);
        };
        fac.getThaiTime = function (dateTime) {
            var d = new Date(fac.parseDate(dateTime));
            if(isNaN(d.getHours())) return ''; // prevent flashing NaN
            var hour = d.getHours();
            if(hour < 10) hour = '0' + hour;
            var min = d.getMinutes();
            if(min < 10) min = '0' + min;
			return hour + '.' + min + ' น.';
        };
        fac.getThaiDateTime = function (dateTime) {
            var d = new Date(dateTime);
            if(isNaN(d.getMonth())) return ''; // prevent flashing NaN 
            var monthLabels = ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.', 'พ.ย.','ธ.ค.'];
			// return d.getDate() + ' ' +  monthLabels[d.getMonth()] + ' ' + (d.getYear() - 100 + 43);
			return d.getDate() + ' ' +  monthLabels[d.getMonth()] + ' ' + (d.getFullYear() + 543) + ' ' + d.getHours() + '.' + d.getMinutes() + ' น.';
        };
        // Platform
        fac.isIOS = function() {
            ionic.Platform.isIOS();
        };
        fac.isIPad = function() {
            ionic.Platform.isIPad();
        };
        fac.isAndroid = function() {
            ionic.Platform.isAndroid();
        };
        fac.isWebView = function() {
            ionic.Platform.isWebView();
        };
        fac.isDevice = function() {
            return ionic.Platform.isIOS() || ionic.Platform.isAndroid() || ionic.Platform.isIPad();
        };
        return fac;
    })
;


