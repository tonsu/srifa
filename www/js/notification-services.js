angular.module('main.services')
	.factory('SrifaNotification', function ($rootScope, $ionicModal, $timeout, $state, $location, RestApi, $ionicLoading, SemiPopup, Helper, SemiStorage, $q, $ionicSideMenuDelegate, $interval, $ionicHistory) {
		var fac = {};

		fac.startUpdateInbox = function (Fetcher) {
			// use current time - 1 hour for making sure there are no unlimited fetch
			var currentTime = new Date();
			var minTime = currentTime.getTime() - (1000 * 60 * 60 * 2); // milliseconds
			// var minTime = currentTime.getTime() - (1000 * 60 * 60 * 24 * 3); // milliseconds
			var minTimeStorage = SemiStorage.get('last_message_time');
			if(!parseInt(minTimeStorage)) {
				minTimeStorage = 0;
			}
			if(minTimeStorage > minTime) {
				minTime = minTimeStorage;
			}

			console.log('[Inbox] storage time:', new Date(minTimeStorage));
			console.log('[Inbox] min time:', new Date(minTime));
			console.log('$rootScope.user', $rootScope.user);
			
			// #1: list all inbox
			RestApi.get('http://srifaapp.appprompt.com/api/message/get_contact', {
				user_id: $rootScope.user.id
			}).then(function(response) {
				var inbox = response.data.result;
				var latestTimeOverall = 0;
				var requests = [];
				// for(var i in inbox) {
				// var currentInbox = inbox[i];
				angular.forEach(inbox, function (currentInbox) {
					if(Helper.parseDate(currentInbox.toTime).getTime() > minTime) {
						var deferred = $q.defer();
						requests.push(deferred.promise);
						RestApi.get('http://srifaapp.appprompt.com/api/message/get_message', {
							user: $rootScope.user.id,
							contact: currentInbox.id
						}).then(function(response) {
							// get only last message (lazy way)
							var messages = response.data.result;
							var lastMessage = messages[messages.length - 1];
							var lastMessageTime = Helper.parseDate(lastMessage.toTime).getTime();
							console.log('[Inbox] * latest message time:', new Date(lastMessageTime));
							if (lastMessageTime > minTime) {
								console.log('1', 1);
								if(lastMessageTime > latestTimeOverall) {
									console.log('2', 2);
									latestTimeOverall = lastMessageTime;
								}
							}
							deferred.resolve();
						}, function(error) {
							deferred.resolve();
							// todo: check error
							console.log('- chat: message error', error);
						});
					}
				});

				$q.all(requests).then(function () {
					console.log('[Inbox set]: latest time', new Date(latestTimeOverall));
					// store latest time
					if(latestTimeOverall > minTimeStorage) {
						// SemiStorage.set('last_message_time', latestTimeOverall);
						console.log('[Inbox set]: latest time', new Date(latestTimeOverall));
					}
				});
				if(Fetcher) Fetcher.finish(); // <-- N.B. You MUST called #finish so that native-side can signal completion of the background-thread to the os.
			}, function(error) {
				// todo: check error
				console.log('- chat: inbox error', error);
				if(Fetcher) Fetcher.finish(); // <-- N.B. You MUST called #finish so that native-side can signal completion of the background-thread to the os.
			});

			// #2: check every inbox
		};

		return fac;
	})
;



