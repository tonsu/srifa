// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('main', [
    'ionic',
    'main.controllers',
    'main.services',
    'main.directives',
    'semi.services',
    'semi.directives',
    'semi.filters'
])

    .run(function ($ionicPlatform, RestApi, $rootScope, $location, SrifaAuth, $state, SemiPopup, $timeout) {

        $rootScope.settings = {
            // mode: 'dev',
            mode: 'production',
            useBackgroundLocation: true,
            test: {
                noData: false // to show how it looks when there is no records (when calling API)
            },
            messageInterval: 6000,
            uuidInterval: 20000,
            // uuidInterval: 6000,
            chatInterval: 6000,
            loginAfterRegister: true
        };

        // Global Variables
        // ----------------------------------------
        $rootScope.isTracking = false;
        $rootScope.deviceReady = false;

        // Common Functions
        // ----------------------------------------
        
        $rootScope.navigate = function(link) {
            // console.log('$location.url() link', $location.url() link);
            // console.log($location.url() + '/' + link);
            // $location.path($location.url() + '/' + link);
            $location.path(link);
        };

        // Background Location
        // ----------------------------------------

        $rootScope.startTracking = function() {
            if($rootScope.deviceReady && $rootScope.settings.useBackgroundLocation && backgroundGeolocation /*&& !$rootScope.isTracking*/) {
                console.log('startTracking');

                // --- track only member
                if(!($rootScope.user.id && $rootScope.user.membercode)) {
                    return;
                }
                backgroundGeolocation.isLocationEnabled(function(isEnabled) {
                    // todo: alert user if not enable tracking
                });
                // BackgroundGeolocation is highly configurable. See platform specific configuration options
                backgroundGeolocation.configure(callbackFn, failureFn, {

                    // stationaryRadius: 20,
                    // distanceFilter: 30,
                    // interval: 60000,

                    // stationaryRadius: 1,
                    // distanceFilter: 1,
                    // interval: 5000,

                    stationaryRadius: 20,
                    distanceFilter: 20,
                    interval: 120000,
                    desiredAccuracy: 10
                });
                // Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
                backgroundGeolocation.start();
                $rootScope.isTracking = true;

                // --- Functions

                var callbackFn = function (location) {
                    // --- Our Test Server
                    // console.log('***** [js] BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);
                    // RestApi.get('http://mspinfo.net/api-tester/public/api/set-location', {
                    //     userid: $rootScope.user.id,
                    //     lat: location.latitude,
                    //     lng: location.longitude
                    // });

                    // --- Srifa Server
                    if($rootScope.user.id && $rootScope.user.id.length > 1) {
                        RestApi.post('http://srifaapp.appprompt.com/api/location/action_add_location', {
                            userid: $rootScope.user.id,
                            lat: location.latitude,
                            log: location.longitude
                        }).then(function(response) {
                            console.log('bg - location:', JSON.stringify(response));
                        });
                    }

                    /*
                     IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
                     and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
                     IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
                     */
                    backgroundGeolocation.finish();
                };
                var failureFn = function (error) {
                    console.log('BackgroundGeolocation error');
                };
            }
        };

        $rootScope.stopTracking = function() {
            console.log('stopTracking');
            if($rootScope.deviceReady && $rootScope.settings.useBackgroundLocation && backgroundGeolocation /*&& !$rootScope.isTracking*/) {
                backgroundGeolocation.stop();
                $rootScope.isTracking = false;
            }
        };

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            document.addEventListener('deviceready', onDeviceReady, false);

            // Plugins Initialization here
            function onDeviceReady() {

                $rootScope.deviceReady = true;
                console.log('device ready!');

                if(window.plugins.OneSignal) {
                    console.log('plugin ready: OneSignal');
                    window.plugins.OneSignal
                        .startInit("40e3b8bd-ad5e-41ba-be8d-f87ac4f5ce38", "991069039710")
                        .handleNotificationReceived(function(jsonData) {
                            // Note: android will alert without having to use SemiPopup
                            // TargetURL: jsonData.payload.additionalData.targetUrl

                            // SemiPopup.alert('<div class="text-center">เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง + JSON.stringify(err)</div>');
                            // alert("Notification received:\n" + JSON.stringify(jsonData, null, 2));
                            // if(jsonData) {
                            //     SemiPopup.alert({
                            //         title: jsonData.payload.title,
                            //         template: '<div class="text-center">' + jsonData.payload.body + '</div>'
                            //     });
                            // }

                            if(jsonData) {
                                SemiPopup.confirm({
                                    title: jsonData.payload.title,
                                    template: '<div class="text-center">' + jsonData.payload.body + '</div>',
                                    okText: 'เปิดอ่าน',
                                    cancelText: 'ปิด'
                                }).then(function (isOk) {
                                    if (isOk) {
                                        var data = jsonData.payload.additionalData;
                                        var segments = data.targetUrl.split('/');
                                        $state.go('app.posts-content', {
                                            postType: segments[0],
                                            id: segments[1]
                                        });
                                    }
                                });
                            }

                            // console.log('handleNotificationReceived: ' + JSON.stringify(jsonData));
                        })
                        .handleNotificationOpened(function(result) {
                            var data = result.notification.payload.additionalData;
                            if (data && data.targetUrl) {
                                // var state = $injector.get($state);
                                // state.go(data.targetUrl);
                                var segments = data.targetUrl.split('/');
                                $state.go('app.posts-content', {
                                    postType: segments[0],
                                    id: segments[1]
                                });
                            }
                        })
                        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
                        .endInit();

                    window.plugins.OneSignal.registerForPushNotifications();
                }

                // Note: background geolocation moved to afterLoginSuccess()
            }
        });

        // For development on browser (refresh without having to login)
        // ----------------------------------------
        $rootScope.lastUrl = $location.url();
        // console.log('$rootScope.lastUrl', $rootScope.lastUrl);
        $rootScope.initialized = false;
    })
;


