angular.module('main.services', [])
	.factory('SrifaAuth', function ($rootScope, $ionicModal, $timeout, $state, $location, RestApi, $ionicLoading, SemiPopup, Helper, SemiStorage, $ionicSideMenuDelegate, $interval, $ionicHistory) {
		var fac = {};

		fac.skipLogin = function () {
			fac.setSkipLoginUser();
			setContainerClasses();
			$state.go("app.home", {}, {location: "replace", reload: true});
		};

		fac.setSkipLoginUser = function () {
			$rootScope.user = {
				id: "anonymous",
				surname: "ผู้ใช้ทั่วไป",
				lastname: "",
				username: "normal_user",
				email: "",
				carnumber: "",
				imgprofile: "/img/default-profile-img.png",
				imgsignature: "",
				barcode: "",
				membercode: "SRIFA00001",
				authid: "4",
				address: '',
				company: '',
				cartype: '',
				anonymous: true
			};
			console.log('skip login user: ', $rootScope.user);
		};

		fac.logout = function (noConfirm) {
			if(noConfirm) {
				// --- Background Geolocation
				$rootScope.stopTracking();
				SemiStorage.clear(); // destroy user from storage
				if($rootScope.uuidCheckInterval) {
					$interval.cancel($rootScope.uuidCheckInterval);
				}
				$state.go("login");
			} else {
				$rootScope.user = null;
				SemiPopup.confirm({
					title: 'Logout',
					template: '<div style="text-align: center">คุณกำลังออกจากระบบ!</div>'
				}).then(function (res) {
					if (res) { // clicked OK
						$ionicSideMenuDelegate.toggleLeft(false); // close left menu
						// --- Background Geolocation
						$rootScope.stopTracking();
						if($rootScope.uuidCheckInterval) {
							$interval.cancel($rootScope.uuidCheckInterval);
						}
						SemiStorage.clear(); // destroy user from storage
						$state.go("login");
					} else {
						// stay the same
					}
				});
			}
		};

		// Facebook Login
		// ----------------------------------------

		fac.facebookLogin = function (fb_id) {
			if (facebookConnectPlugin) {
				facebookConnectPlugin.logout(function() {
					// success
				}, function() {
					// failed
				});
				facebookConnectPlugin.getLoginStatus(function (response) {
					// Need to use facebookConnectPlugin.login() if the status is unknown
					// ref: https://github.com/jeduan/cordova-plugin-facebook4/issues/109
					console.log('qwer', JSON.stringify(response));
					if (response.status === 'connected') {
						console.log('- fbapp: ', JSON.stringify(response));
						loginByFacebookID(response.authResponse.userID);
					} else {
						facebookConnectPlugin.login(['email', 'public_profile'], function (response) {
							console.log('- facebookConnectPlugin.login: ', JSON.stringify(response));
							loginByFacebookID(response.authResponse.userID, response.authResponse);
						}, function (err) {
							console.log('err', err);
							SemiPopup.alert('<div class="text-center">เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง</div>');
						});
					}
				}, function (err) {
					console.log('fbStatusFail', JSON.stringify(err));
				});
			} else {
				console.log('---**--- no facebookConnectPlugin');
			}
		};

		function loginByFacebookID(fb_id, authResponse) {
			// alert('loginByFacebookID');
			$ionicLoading.show();

			// UUID
			var uuid = 'devtest';
			if(window.device && window.device.uuid) {
				uuid = window.device.uuid;
			}
			console.log('[login - facebook id] uuid', uuid);

			RestApi.post('http://srifaapp.appprompt.com/api/facebook/action_fb_login', {
				fb_id: fb_id,
				uuid: uuid
			}).then(function (response) {
				console.log('facebook login check:', JSON.stringify(response));
				$ionicLoading.show();

				// Already registered, logged in!
				if (response.data.result && response.data.result[0]) {
					$rootScope.user = response.data.result[0];
					storeLoginData(false, false, fb_id);
					afterLoginSuccess();
					$state.go("app.home", {}, {location: "replace", reload: true});
				}
				// error
				// auto login do not show error, just redirect to login
				else if (response.data.status && authResponse != 'auto-login') {
					SemiPopup.alert('<div class="text-center">'+response.data.status+'</div>');
				}
				// Not registered, redirect to register
				else {
					if (authResponse && authResponse != 'auto-login') {
						fetchFbUserDataAndRegister(authResponse);
					}
					// when this function called by auto login
					else {
						$state.go("login", {}, {location: "replace", reload: true});
					}
				}
			}).finally(function () {
				$ionicLoading.hide();
			});
		}

		function fetchFbUserDataAndRegister(authResponse) {
			// console.log('1.authResponse', JSON.stringify(authResponse));
			// console.log('authResponse.accessToken', authResponse.accessToken);
			facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
				function (response) {
					console.log('fetchFbUserDataAndRegister success: ', JSON.stringify(response));
					var fullName = response.name.split(' ');
					console.log('response.name', response.name);
					var firstName = fullName[0];
					var lastName = fullName[1] || '';
					$rootScope.fbRegisterParams = {
						fbId: authResponse.userID,
						email: response.email,
						firstName: firstName,
						lastName: lastName
					};
					// $state.go("register-facebook", {isFacebook: true}, {reload: true});
					$state.go("register", {reload: true});
				},
				function (error) {
					console.log('fetchFbUserDataAndRegister error: ', JSON.stringify(error));
					SemiPopup.alert('<div class="text-center">เกิดข้อผิดพลาดระหว่างการขอข้อมูลจาก Facebook</div>').then(function() {
						// Just ignore data and go to register
						$state.go("register");
					});
				}
			);
		}

		// Login Functions
		// ----------------------------------------

		fac.login = function (username, password, isAutoLogin) {
			if ($rootScope.settings.mode == 'production') {
				$ionicLoading.show();

				// UUID
				var uuid = 'devtest';
				if(window.device && window.device.uuid) {
					uuid = window.device.uuid;
				}
				console.log('[login] uuid', uuid);
				console.log('[login] user/pass', username, password);

				RestApi.post('http://srifaapp.appprompt.com/api/login/get_action_login', {
					// user_id: $rootScope.user.id
					username: username,
					password: password,
					uuid: uuid
				}).then(function (response) {
					$ionicLoading.hide();
					if (response.data.result && response.data.result[0]) {
						$rootScope.user = response.data.result[0];
						storeLoginData(username, password);
						afterLoginSuccess();
					} else {
						if(isAutoLogin) {
							$state.go("login", {}, {location: "replace", reload: true});
						} else {
							SemiPopup.alert('<div class="text-center">ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง</div>');
						}
					}
				}).finally(function () {
					$ionicLoading.hide();
				});
			} else {
				// todo: dev mode code
				$state.go("app.home", {}, {location: "replace", reload: true});
			}
			// $location.path('/app/posts/news');
		};

		// After Login Functions
		// ----------------------------------------

		function afterLoginSuccess() {
			console.log('After Login Success');

			// Resolve Promise
			$rootScope.loginDefer.resolve();

			// check UUID using interval
			if($rootScope.uuidCheckInterval) {
				$interval.cancel($rootScope.uuidCheckInterval);
			}
			// UUID
			var uuid = 'devtest';
			if(window.device && window.device.uuid) {
				uuid = window.device.uuid;
			}
			$rootScope.uuidCheckInterval = $interval(function() {
				RestApi.post('http://srifaapp.appprompt.com/api/login/get_check_user_online', {
					userid: $rootScope.user.id,
					uuid: uuid
				}).then(function (response) {
					console.log('uuid check: ', response.data.status, uuid);
					if (response.data.status != true) {
						if($rootScope.uuidCheckInterval) {
							$interval.cancel($rootScope.uuidCheckInterval);
						}
						SemiPopup.alert('<div class="text-center">คุณได้เข้าสู่ระบบด้วยอุปกรณ์อื่น</div>').then(function() {
						// SemiPopup.alert('<div class="text-center">คุณได้เข้าสู่ระบบด้วยอุปกรณ์อื่น<br/>'+$rootScope.user.id+'<br/>'+uuid+'</div>').then(function() {
							fac.logout(true);
						});
					}
				})
			}, $rootScope.settings.uuidInterval);

			// User
			// ----------------------------------------
			// Prevent becoming anonymous
			$rootScope.user.anonymous = false;

			// Plugins
			// ----------------------------------------
			// --- OneSignal
			if (window.plugins && window.plugins.OneSignal) {
				console.log('send to OneSignal');
				window.plugins.OneSignal.sendTags({
					'auth_id' : $rootScope.user.authid,
					'userid' : $rootScope.user.id
				});
			}
			// --- Background Geolocation
			$rootScope.startTracking();

			// Other
			// ----------------------------------------
			fetchSrifaData();
			setContainerClasses();
			// console.log('------- debug init -------');
			// console.log('$rootScope.lastUrl', $rootScope.lastUrl);

			if($rootScope.lastUrl && !window.plugins) {
				if($rootScope.lastUrl == '/init') {
					// preventing android back button
					$state.go("app.home", {}, {location: "replace", reload: true});
				} else {
					$location.path($rootScope.lastUrl, {}, {location: "replace", reload: true});
				}
				$rootScope.lastUrl = false;
			} else {
				$state.go("app.home", {}, {location: "replace", reload: true});
			}
		}

		// Hides menu & limits functionality of anonymous user
		function setContainerClasses() {
			if ($rootScope.user.anonymous) {
				$rootScope.containerClasses = 'anonymous';
			} else {
				$rootScope.containerClasses = '';
			}
		}

		function fetchSrifaData() {
			// Commission: sum month
			var now = new Date();
			RestApi.get('http://srifaapp.appprompt.com/api/transaction/get_transaction', {
				start: now.getMonth(), // #month
				end: now.getMonth() + 1,
				member: $rootScope.user.membercode
			}).then(function (response) {
				$rootScope.commissionSumAll = '';
				$rootScope.commissionSumMonth = '';
				if (response.data.commission_all) $rootScope.commissionSumAll = response.data.commission_all[0].sum;
				if (response.data.commission_by_month) $rootScope.commissionSumMonth = response.data.commission_by_month[0].sum;
			});

			// Get main branch info
			RestApi.get('http://srifaapp.appprompt.com/api/branch/get_branch').then(function (response) {
				$rootScope.srifa = response.data.result[0];
			}, function (err) { // error
			}).finally(function () {
				$ionicLoading.hide();
			});
		}

		function storeLoginData(username, password, fb_id) {
			if (fb_id) {
				SemiStorage.clear();
				SemiStorage.set('fb_id', fb_id);
			} else {
				SemiStorage.clear();
				SemiStorage.set('username', username);
				SemiStorage.set('password', password);
			}

			// Test Storage
			// SemiStorage.set('test', 123456);
			// console.log('SemiStorage.get(test)', SemiStorage.get('test'));
			// SemiStorage.clear();
			// console.log('SemiStorage.get(test)', SemiStorage.get('test'));
		}

		// Initialize
		// ----------------------------------------

		fac.initializeApp = function() {
			console.log('--- Initialization ---');
			// Only init once
			if(!$rootScope.initialized) {
				// console.log('init');
				// console.log('$rootScope.settings', $rootScope.settings);

				if ($rootScope.settings.mode != 'dev') {
					// Auto Login on start
					// ----------------------------------------
					console.log('[auto login] fb_id', SemiStorage.get('fb_id'));
					console.log('[auto login] username', SemiStorage.get('username'));
					console.log('[auto login] password', SemiStorage.get('password'));
					var fb_id = SemiStorage.get('fb_id');
					if (fb_id) {
						// alert(fb_id);
						loginByFacebookID(fb_id, 'auto-login');
					} else {
						var username = SemiStorage.get('username');
						var password = SemiStorage.get('password');
						if (username && password) {
							fac.login(username, password, true);
						}
						// cannot auto login (no previous login data stored)
						else {
							// go to login page
							// $state.go('login');

							// Note: changed to auto skip
							fac.skipLogin();
						}
					}
				} else {
					// Dev: mock logged in user
					// $rootScope.user = {
					// 	id: "usr0002",
					// 	surname: "นายสมศักด์",
					// 	lastname: "กินอะไรดี",
					// 	username: "srifa1",
					// 	email: "",
					// 	tel: "08912345678",
					// 	carnumber: "กบว123",
					// 	imgprofile: "http://srifaapp.appprompt.com/uploads/user/usr0001.jpg",
					// 	imgsignature: "http://srifaapp.appprompt.com/uploads/user/",
					// 	barcode: "http://srifaapp.appprompt.com/uploads/barcode/srifa00001.gif",
					// 	membercode: "SRIFA00001",
					// 	authid: "1",
					// 	address: '-',
					// 	company: '-',
					// 	cartype: '-',
					// 	anonymous: false
					// };
					fac.skipLogin();
				}
				$rootScope.initialized = true;
			}
		};
		return fac;
	})
;



